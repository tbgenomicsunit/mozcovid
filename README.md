## MozCOVID_commands.txt

Contains all commands used to perform SARS-CoV-2 sequences alignment, reconstruct the phylogenies with UShER and visualize them in Taxonium, and perform the SH test with IQ-TREE.

## script_scan_clades.R

This is the script used to find all transmission groups and unique introductions within a given phylogeny.
